import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class SpellCheckerTest {
	
	public static void main(String[] args) throws IOException {
		SpellChecker spellcheck = new SpellChecker();
		
		Scanner io = new Scanner(System.in);
		String input;
		
		input = io.nextLine();
		
		io.close();
		
		// This works as a basic example but we'll want to expand it
		// a little to make it a little more robust. For one, we will
		// want to strip out any punctuation that will throw off the
		// spell checker. I've already applied toLowerCase() to everything
		// to avoid false-negatives on capitalized words.
		
		for (String word : input.split(" ")) {
			word = word.toLowerCase();
			word = word.replaceAll("\\p{Punct}", "");
			
			if (!spellcheck.checkWord(word.toLowerCase())) {
				System.out.println(word + " is spelled wrong.");
				ArrayList<String> suggestions = spellcheck.suggestWords(word.toLowerCase());
				if (suggestions.size() > 0) System.out.println("Did you mean?");
				for (String s : suggestions) {
					System.out.println("\t" + s);
				}
			}
			else {
				System.out.println(word + " is spelled correctly");
			}
		}
	}

}