import java.awt.*;
import java.io.IOException;

import javax.swing.*;

public class WordProcessor {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	private static JFrame window = new JFrame();
	private static JPanel panel = new JPanel();
	private static JTextPane paper = new JTextPane();
	private static SpellChecker spellcheck;
	private static JPopupMenu suggestions = new JPopupMenu();

	public static void main(String[] args) throws IOException {
		//	Initialize our SpellChecker object, this will give us
		//	an object which we can use to check the spelling of a
		//	given word and, if it is mis-spelled, we can retrieve
		//	a list of words closely matching the word we supplied.
		spellcheck = new SpellChecker();
		
		//	Initialize the JFrame object
		//	+ When the user closes the frame, exit the application
		// 	+ Set the size to 1024x768px
		//	+ Center the JFrame on the desktop
		//	+ Remove the default layout manager (if any) and force
		//	  absolute positioning of components
		//	+ Don't allow the user to resize the frame
		//	+ Set the title of the window
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(1024, 768);
		window.setLocationRelativeTo(null);
		window.setLayout(null);
		window.setResizable(false);
		window.setTitle("Jeffrey and Joseph's Awesome Spell Checker - CSIS 2420");
		
		//	Initialize the containing JPanel
		//	+ Set the size to 1024x768px (this allows for differently
		//	  sized borders around the JFrame on different operating systems)
		//	+ Set the location to the top left corner of the JFrame
		//	+ Remove the default layout manager (if any) and force
		//	  absolute positioning of components
		//	+ Set the background color to gray to contrast the white JTextPane
		//	+ Add the panel to our JFrame
		panel.setSize(1024, 768);
		panel.setLocation(0, 0);
		panel.setLayout(null);
		panel.setBackground(Color.GRAY);
		window.add(panel);
		
		//	Initialize our "Paper" (JTextPane)
		//	+ Set the size to 900x625px
		//	+ Set the location, providing an adequate margin within the JPanel
		//	+ Set appropriate margins within the JTextPane for easy readability
		//	+ Set the font to a sensible default that is easy to read and large
		//	  enough to be seen from a distance on any screen (for demo purposes)
		//	+ Set the SpellCheck object as the KeyListener for this component
		//	+ Set the SpellCheck object as the MouseListener for this component
		//	+ Set the default JPopupMenu for this component, used to suggest
		//	  words to replace the mis-spelled ones within the JTextPane
		//	+ Add the JTextPane to the containing JPanel
		paper.setSize(900, 625);
		paper.setLocation(56, 54);
		paper.setMargin(new Insets(40, 50, 40, 50));
		paper.setFont(new Font("Arial", 0, 24));
		paper.addKeyListener(spellcheck);
		paper.addMouseListener(spellcheck);
		paper.setComponentPopupMenu(suggestions);
		panel.add(paper);
		
		//	Show the JFrame to the user now that everything has been
		//	initialized and is ready to go.
		window.setVisible(true);
	}

}