import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.*;

import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.text.*;
import javax.swing.text.Highlighter.*;

public class SpellChecker implements KeyListener, MouseListener {
	private HashSet<String> dictionary = new HashSet<String>();
	private ArrayList<String> closeWords = new ArrayList<String>();
	private JTextPane source;
	
	public SpellChecker() throws IOException {
		// We use the constructor to automatically load the dictionary
		// when the class is instantiated so it's ready to use
		// immediately thereafter. It takes about 150ms (see notes below)
		// but this doesn't seem to be an inordinate amount of time for most
		// use cases (i.e. as part of a word processing application)
		this.loadWordList();
	}
	
	/**
	 * @throws IOException
	 * 
	 * loadWordList()
	 * 
	 * This function is called within the constructor and is used
	 * as a convenience method to read the dictionary from file
	 * and populate the HashSet<String> that represents our
	 * dictionary in memory.
	 * 
	 * The current dictionary we are using (words_1.txt) contains
	 * approximately 153,000 common English words.
	 * 
	 * There are multiple (though similar) methods to read the
	 * file contents into the HashSet but both are similar in
	 * the amount of time they take to populate the object so
	 * an advantage has no been found in using one over the other.
	 * 
	 */
	private void loadWordList() throws IOException {
		long _startFile = System.currentTimeMillis();
		
		// Read the file "words.txt" from the filesystem to a byte
		// array and then convert it to a String for easier manipulation
		FileInputStream file = new FileInputStream("words.txt");
		byte[] b = new byte[file.available()];
		file.read(b);
		file.close();
		
		String contents = new String(b);
		long _endFile = System.currentTimeMillis();
		
		// Method 1:	Collection.addAll()
		// 				This method takes approximately 200ms (on average)
		//				to load the file into the HashSet dictionary.
		//
		// this.dictionary.addAll(Arrays.asList(contents.split("\r\n")));
		
		long _startToMem = System.currentTimeMillis();
		
		// Method 2:	Collection.add() combined with iteration
		//				This method takes approximately 180ms (on average)
		//				to load the file into the HashSet dictionary.
		// 				
		//				I suspect this is what is happening behind the scenes of
		// 				the addAll() method, hence it takes the same amount
		//				of time with a little bit of extra overhead (~20ms)
		for (String word : contents.split("\n")) {
			this.dictionary.add(word);
		}
		
		long _endToMem = System.currentTimeMillis();
		
		System.out.println((_endFile - _startFile) + "ms to load " + this.dictionary.size() + " words from the file");
		System.out.println((_endToMem - _startToMem) + "ms to store all words in the HashSet dictionary");
	}

	/**
	 * 
	 * 	@param 	word <String>
	 * 		The word to be checked against the dictionary
	 * 	
	 * 	@return <boolean>
	 * 		Returns a boolean indicating whether the word supplied exists
	 * 		in the dictionary (true) or not (false).
	 * 
	 * 	This function checks both the word as passed to the function
	 *  and the word converted to lower case so as to avoid false negatives
	 *  on words supplied from the beginning of a sentence.
	 *  
	 */
	public boolean checkWord(String word) {
		return (this.dictionary.contains(word) || this.dictionary.contains(word.toLowerCase()));
	}
	
	/**
	 * 
	 * 	@param	misspelled <String>
	 * 		A word that has been determined to be mis-spelled by the checkWord function
	 * 		for which we want to receive suggestions.
	 * 	
	 * 	@return <boolean>
	 * 		Returns an ArrayList<String> of words from the dictionary HashSet that are
	 * 		most similar (least expensive) to the word supplied as the argument.
	 * 
	 * 	This function uses a version of the Levenshtein distance algorithm to determine
	 * 	the "cost" or distance between two given words and return an abbreviated list with
	 * 	the least expensive (or most similar) words as suggestions for the correct spelling of
	 *	the word intended by the user.
	 *
	 *	The Levenshtein distance algorithm is very useful but has a few noted drawbacks when
	 *	used in the context of a spell checking application. An n-gram distance algorithm
	 *	would have been better suited for the task, but it comes at the cost of greater
	 *	complexity and greater time to implement.
	 * 
	 */
	public ArrayList<String> suggestWords(String misspelled) {
		// We use a TreeMap because it inherits from SortedMap so it will be sorted automatically
		// for us when we go to use it later on, this is rather useful when used correctly.
		//
		// We make it the key an Integer because the cost, as calculated by the Levenshtein
		// distance algorithm is returned as an Integer and we naturally want to sort in
		// ascending order on that key to find the most similar words to the one provided.
		//
		// We use an ArrayList<String> as the value because where the key is the cost or distance
		// between two given strings, there can be multiple words with the same distance and we
		// want to preserve each of those words, rather than replacing or dropping them (as in a Set)
		TreeMap<Integer, ArrayList<String>> map = new TreeMap<Integer, ArrayList<String>>();
		
		// We iterate over each word in the dictionary HashSet and calculate the Levenshtein
		// distance between the provided mis-spelled word and the word from the dictionary.
		// We store each word in an ArrayList within the TreeMap we initialized above with
		// the key being it's calcuated distance from the provided word.
		for (String word : this.dictionary) {
			int cost = this.getDistance(word, misspelled);
			
			// Check if there's already an ArrayList started for this particular distance/cost
			// If not, start a new one and insert the first word
			if (map.get(cost) == null) {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(word);
				map.put(cost, temp);
			}
			// If one already exists, add the current word to the ArrayList
			else {
				map.get(cost).add(word);
			}
		}
		
		// Since the TreeMap is naturally sorted by the key in ascending order we don't
		// need to do anything except grap the first Entry in the Map and return it's value,
		// the ArrayList of words that all fall under the same distance.
		return map.firstEntry().getValue();
	}
	
	/**
	 * 
	 * 	@param source <JTextPane>
	 *		The JTextPane from the WordProcessor that contains the text to be checked for spelling errors
	 * 
	 *	This function takes the text from our WordProcessor and checks each word for correctness againts
	 *	our already initialized dictionary. If a mis-spelled word is found it is highlighted in red so
	 *	the user knows he/she has made a mistake.
	 * 
	 */
	public void checkText(JTextPane source) {
		// The highlighter attached to the JTextPane, used to highlight mis-spelled words
		Highlighter highlighter = source.getHighlighter();
		
		// Create the Painter used by the Highlighter and initialize it with a custom highlight color (light red)
		HighlightPainter redPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.decode("#efbdbd"));
		
		// Remove any pre-existing highlights (to prevent putting multiple highlights on the same word)
		highlighter.removeAllHighlights();
		
		// Initialize an ArrayList<int[]> to hold the start and end index of each word that is spelled wrong
		// This is used later to add each highlight to the JTextPane between the correct indices.
		ArrayList<int[]> wrongWords = new ArrayList<int[]>();
		
		// Get the text from the JTextPane component
		String text = source.getText();
		
		// Index placeholder, this is necessary if we have duplicate words within
		// the text, because the indexOf() method with only return the first
		// instance of the word unless we provide a starting point. This also
		// allows the function to process large amounts of text much faster
		// because it doesn't need to process any text that has already
		// been processed even though it is an iterative loop.
		int currentIndex = 0;
		
		// Loop through each word within the text and process it, removing
		// any trailing punctuation (end of sentences or pauses within a sentence)
		// and check to see if it's spelled correctly. If this.checkWord() returns
		// false then add the start and end indices of the word to our wrongWords
		// ArrayList. When we have determined all the incorrect words, highlight
		// them in the JTextPane so the user can take corrective action.
		for (String word : text.split(" ")) {
			int start = source.getText().indexOf(word, currentIndex);
			
			// With this regex we only want to replace/remove
			// punctuation that is at the END of the word
			// (i.e. .,!?;) that denote the end of a sentence
			// not other punctuation within the word because
			// our dictionary accounts for those words (contractions, etc.)
			word = word.replaceAll("\\p{Punct}$", "");
			
			int end = (start + word.length());
			
			if (!this.checkWord(word)) {
				int[] temp = {start, end};
				wrongWords.add(temp);
			}
			
			currentIndex = (end - 1);
		}
		
		// Highlight each offending word found above
		try {
			for (int[] offsets : wrongWords) {
				highlighter.addHighlight(offsets[0], offsets[1], redPainter);
			}
		}
		catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 	
	 * 	Levenshtein distance between two Strings
	 *	See http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Java
	 *	for more information on the Levenshtein distance algorithm
	 * 
	 *	@param str1 <String>
	 *		The first of two strings to calculate the cost or distance between
	 *
	 * 	@param str2 <String>
	 * 		The second of two strings to calculate the cost or distance between
	 * 
	 * 	@return <int>
	 * 		Returns an integer >= 0 that is the calculated cost between str1 and str2.
	 * 		The smaller the number the closer the match between the two strings.
	 * 
	 * 		If 0, the two strings are the same.
	 *
	 */
	public int getDistance(String str1,String str2) {
		int[][] distance = new int[str1.length() + 1][str2.length() + 1];
 
		for (int i = 0; i <= str1.length(); i++) {
			distance[i][0] = i;
		}
		
		for (int j = 1; j <= str2.length(); j++) {
			distance[0][j] = j;
		}
 
		for (int i = 1; i <= str1.length(); i++) {
			for (int j = 1; j <= str2.length(); j++) {
				distance[i][j] = minimum((distance[i - 1][j] + 1), (distance[i][j - 1] + 1), (distance[i - 1][j - 1] + ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0 : 1)));
			}
		}
 
		return distance[str1.length()][str2.length()];
	}

	/**
	 * 
	 *	Used to calculate the minimum integer of three integers
	 *	Used solely within the getDistance() method
	 *
	 * 	@param a <int>
	 * 		Integer 1
	 *
	 *	@param b <int>
	 *		Integer 2
	 *
	 * 	@param c <int>
	 * 		Integer 3
	 * 
	 * 	@return <int>
	 * 		Return the minimum integer of the three provided integers
	 * 
	 */
	private int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// When the space key is pressed and released, re-check the text
		// in the JTextPane for any mis-spelled words and highlight them.
		if (e.getKeyCode() == 32) {
			this.checkText((JTextPane) e.getSource());
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// If the mouse was right clicked on the JTextPane take appropriate measures
		// to prepare to show the JPopupMenu populated with similar words to the
		// mis-spelled on the user right clicked on.
		if (e.getComponent().getClass().toString().equals("class javax.swing.JTextPane")) {
			this.source = (JTextPane)e.getComponent();
			
			// Only react to right clicks
			if (e.getButton() == 3) {
				// A necessary "trick" in order to translate the pixel location of the mouse
				// click to the index/caret location of the word within the JTextPane
				this.source.setCaretPosition(this.source.viewToModel(e.getPoint()));
				this.source.getComponentPopupMenu().removeAll();
				
				// Loop through each highlight and determine which word/highlight has been
				// clicked on. This is done by determining if the current caret position, set
				// above to the position the user clicked on, is within the bounds of the given
				// highlighted word.
				//
				// When we have our desired word, retrieve the suggested similar words and
				// break from the loop so as not to waste any more time and cpu cycles on
				// unnecessary processing.
				for (Highlighter.Highlight mark : this.source.getHighlighter().getHighlights()) {
					if ((mark.getStartOffset() < this.source.getCaretPosition()) && (mark.getEndOffset() > this.source.getCaretPosition())) {
						String word = "";
						
						try {
							word = this.source.getText(mark.getStartOffset(), (mark.getEndOffset() - mark.getStartOffset())); 
							
							this.closeWords = this.suggestWords(word);
							
							break;
						}
						catch (Exception error) {
							error.printStackTrace();
						}
					}
				}
				
				// Iterate over each suggested word (ArrayList) and create
				// a new JMenuItem for it, attach a mouse listener to listen
				// for the users click, and add it to the JPopupMenu initialized
				// in the WordProcessor.
				for (String str : this.closeWords) {
					JMenuItem temp = new JMenuItem(str);
					temp.addMouseListener(this);
					
					this.source.getComponentPopupMenu().add(temp);
				}
			}
			else if (e.getButton() == 1) {
				// When the user closes the popup menu, remove all menu items from it
				// in preparation of generating the new menu the next time they right click
				// on a highlighted word.
				if (!this.source.getComponentPopupMenu().hasFocus()) {
				 	this.source.getComponentPopupMenu().removeAll();
				}
			}
		}
		// If the user clicked on a JMenuItem, they have selected a word to replace the
		// highlighted word with so we need to once again find that word and replace it
		// with the selected on from the menu. We also need to remove the highlight as
		// the word is no longer spelled incorrectly. This also serves to provide immediate
		// feedback to the user upon clicking on an item so they know their action was
		// received and has been processed (the alternative is replace the text and wait
		// until the next checkWords() cycle to remove the highlight, this isn't very
		// user friendly so it's not really a legitimate option...)
		else if (e.getComponent().getClass().toString().equals("class javax.swing.JMenuItem")) {
			JMenuItem menuItem = (JMenuItem) e.getComponent();
			String newWord = menuItem.getText();
			int start = 0;
			int end = 0;
			
			// Loop through each highlight and find the current word that the user has selected
			// When we find that word, remove the highlight and break from the loop so we
			// can continue to process the request.
			for (Highlighter.Highlight mark : this.source.getHighlighter().getHighlights()) {
				if ((mark.getStartOffset() < this.source.getCaretPosition()) && (mark.getEndOffset() > this.source.getCaretPosition())) {
					try {
						start = mark.getStartOffset();
						end = mark.getEndOffset();
						
						this.source.getHighlighter().removeHighlight(mark);
						
						break;
					}
					catch (Exception error) {
						error.printStackTrace();
					}
				}
			}
			
			// Select the current word and replace the selected text with
			// the word returned from the JMenuItem the user selected.
			this.source.setSelectionStart(start);
			this.source.setSelectionEnd(end);
			this.source.replaceSelection(newWord);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// When the user releases the right mouse button show the JPopupMenu with the options
		// for correcting the mis-spelled word they clicked on.
		if (e.getComponent().getClass().toString().equals("class javax.swing.JTextPane")) {
			if (e.getButton() == 3) {
				this.source.getComponentPopupMenu().show(this.source, e.getX(), e.getY());
			}
		}
	}
	
	
	// Unused mouse and key listeners, necessary to include because we
	// implement the <MouseListener> and <KeyListener> abstract classes
	@Override
	public void keyPressed(KeyEvent e) {}
	
	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
}